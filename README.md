# osrs-packs

[![Actions Status](https://github.com/TSedlar/osrs-packs/workflows/CI/badge.svg?event=schedule)](https://github.com/TSedlar/osrs-packs/actions)

All of the gamepacks listed under [packs](packs/) are automatically uploaded to this repository.

The code for retrieval and labeling of gamepacks can be seen in [OSRSDownloader](OSRSDownloader.groovy)

The most current gamepack is checked for every 30 minutes on Azure as a scheduled CI pipeline build.
